import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.*;

public class Gui {
    public static Generator generator;
    private JFrame frame;
    private ArrayList<JTextArea> casa = new ArrayList<>();
    private static Task task;
    private JTextField min = new JTextField();
    private JTextField max = new JTextField();
    private JTextField minST = new JTextField();
    private JTextField maxST = new JTextField();
    private JTextField simTime = new JTextField();
    private JTextField clients = new JTextField();
    private long startTime;
    private static int msg = 1;
    private static Producator prod = new Producator();
    public int intrerupt = 0;

    private JLabel casa1L = new JLabel();
    private JLabel casa2L = new JLabel();
    private JLabel casa3L = new JLabel();
    private JLabel casa4L = new JLabel();
    private JLabel casa5L = new JLabel();


    public static void main(String[] args) throws InterruptedException {
        Gui gui = new Gui();
        gui.processor();
    }

    private void processor() throws InterruptedException {
        Thread interfata = new Thread(new Runnable() {
            @Override
            public void run() {
                EventQueue.invokeLater(() -> {
                    try {
                        Gui window = new Gui();
                        window.initialize();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        });
        interfata.setName("Interfata");
        interfata.start();

    }

    private void initialize() {
        frame = new JFrame();
        frame.setTitle("Threads");
        frame.setBounds(100, 100, 825, 440);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setVisible(true);

        casa.add(new JTextArea());
        casa.get(0).setBounds(10, 140, 150, 250);

        casa.add(new JTextArea());
        casa.get(1).setBounds(170, 140, 150, 250);

        casa.add(new JTextArea());
        casa.get(2).setBounds(330, 140, 150, 250);

        casa.add(new JTextArea());
        casa.get(3).setBounds(490, 140, 150, 250);

        casa.add(new JTextArea());
        casa.get(4).setBounds(650, 140, 150, 250);

        for (JTextArea text : casa) {
            text.setEditable(false);
            frame.add(text);
        }

        casa1L.setBounds(15, 110, 145, 20);
        casa1L.setText("Casa1 ST: ");

        casa2L.setBounds(175, 110, 145, 20);
        casa2L.setText("Casa2 ST: ");

        casa3L.setBounds(335, 110, 145, 20);
        casa3L.setText("Casa3 ST: ");

        casa4L.setBounds(495, 110, 145, 20);
        casa4L.setText("Casa4 ST: ");

        casa5L.setBounds(655, 110, 145, 20);
        casa5L.setText("Casa5 ST: ");

        JLabel minLabel = new JLabel();
        minLabel.setBounds(10, 10, 70, 20);
        minLabel.setText("Min time: ");
        frame.add(minLabel);


        min.setBounds(70, 10, 50, 20);
        frame.add(min);

        JLabel maxLable = new JLabel();
        maxLable.setBounds(10, 40, 70, 20);
        maxLable.setText("Max time: ");
        frame.add(maxLable);


        max.setBounds(70, 40, 50, 20);
        frame.add(max);

        JLabel minSTLabel = new JLabel();
        minSTLabel.setBounds(150, 10, 70, 20);
        minSTLabel.setText("Min ST: ");
        frame.add(minSTLabel);

        JLabel maxSTLabel = new JLabel();
        maxSTLabel.setBounds(150, 40, 70, 20);
        maxSTLabel.setText("Max ST: ");
        frame.add(maxSTLabel);


        minST.setBounds(200, 10, 50, 20);
        frame.add(minST);

        maxST.setBounds(200, 40, 50, 20);
        frame.add(maxST);

        JLabel simulation = new JLabel();
        simulation.setBounds(300, 10, 75, 20);
        simulation.setText("Sim time (s): ");
        frame.add(simulation);


        simTime.setBounds(400, 10, 75, 20);
        frame.add(simTime);

        JLabel clientsLabel = new JLabel();
        clientsLabel.setBounds(300, 40, 100, 20);
        clientsLabel.setText("Clients Number: ");
        frame.add(clientsLabel);

        clients.setBounds(400, 40, 75, 20);
        frame.add(clients);

        JLabel timeLabel = new JLabel();
        timeLabel.setBounds(680, 10, 70, 20);
        timeLabel.setFont(new Font("Open Sans", 0, 15));

        JButton start = new JButton();
        start.setBounds(670, 10, 70, 20);
        start.setText("Start");
        frame.add(start);

        JButton repeat = new JButton();
        repeat.setBounds(660, 40, 90, 20);
        repeat.setText("Generate");

        repeat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                (task = new Task()).execute();
            }
        });

        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.add(casa1L);
                frame.add(casa2L);
                frame.add(casa3L);
                frame.add(casa4L);
                frame.add(casa5L);

                frame.add(repeat);
                startTime = System.currentTimeMillis();
                if (!(min.getText().isEmpty() && max.getText().isEmpty() && maxST.getText().isEmpty() && minST.getText().isEmpty()
                        && clients.getText().isEmpty() && simTime.getText().isEmpty())) {
                    start.setVisible(false);
                    DateFormat time = new SimpleDateFormat("mm:ss.SSS");
                    long startTime = System.currentTimeMillis();
                    ActionListener timeListener = new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Date date = new Date(System.currentTimeMillis() - startTime);
                            String t = time.format(date);
                            timeLabel.setText(t);
                            msg = 0;

                            if (Integer.parseInt(simTime.getText()) < (int) (System.currentTimeMillis() - startTime) / 1000) {
                                for (Thread th : Thread.getAllStackTraces().keySet()) {
                                    if (th.getState() == Thread.State.RUNNABLE)
                                        th.interrupt();
                                }
                                for (Thread th : Thread.getAllStackTraces().keySet()) {
                                    if (th.getState() == Thread.State.RUNNABLE) {
                                        th.stop();
                                    }
                                }
                                intrerupt = 1;
                            }
                        }
                    };

                    frame.add(timeLabel);
                    javax.swing.Timer t = new javax.swing.Timer(10, timeListener);
                    t.setInitialDelay(0);
                    t.start();

                    (task = new Task()).execute();

                }
            }
        });
    }

    private class Task extends SwingWorker<String, String> {
        @Override
        protected String doInBackground() throws Exception {
            if (msg == 1)
                generator = new Generator(Integer.parseInt(min.getText()), Integer.parseInt(max.getText()), Integer.parseInt(minST.getText()),
                        Integer.parseInt(maxST.getText()), Integer.parseInt(clients.getText()), Integer.parseInt(simTime.getText()));

            Thread gen = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        generator.generate(startTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

            Thread afisare = new Thread(new Runnable() {
                @Override
                public void run() {
                    casa1L.setText(casa1L.getText() + generator.list.getCoada(0).getServiceTime() + "s");
                    casa2L.setText(casa2L.getText() + generator.list.getCoada(1).getServiceTime() + "s");
                    casa3L.setText(casa3L.getText() + generator.list.getCoada(2).getServiceTime() + "s");
                    casa4L.setText(casa4L.getText() + generator.list.getCoada(3).getServiceTime() + "s");
                    casa5L.setText(casa5L.getText() + generator.list.getCoada(4).getServiceTime() + "s");

                    while (true) {
                        for (int i = 0; i < 5; i++) {
//                            System.out.println(generator.list.getCoada(i).toString());
                            casa.get(i).setText(generator.list.getCoada(i).toString());
                        }
                    }


                }
            });
            Thread[] coada = new Thread[5];
            coada[0] = generator.list.getCoada(0);
            coada[1] = generator.list.getCoada(1);
            coada[2] = generator.list.getCoada(2);
            coada[3] = generator.list.getCoada(3);
            coada[4] = generator.list.getCoada(4);


            ExecutorService ex = Executors.newFixedThreadPool(10);
            ex.execute(gen);
            if (msg == 1) {
                ex.execute(afisare);
                for (int i = 0; i < 5; i++) {
                    ex.execute(coada[i]);
                }
            }

            return null;
        }
    }
}
