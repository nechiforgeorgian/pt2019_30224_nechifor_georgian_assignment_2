import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;


public class Generator {
    private static int id = 1;
    private int minAR; //min time between clients arrival
    private int maxAR;
    private int minST;
    private int maxST;
    private int clientsNumber;
    private int simTime;

    Producator list = new Producator();

    Generator(int minAR, int maxAR, int minST, int maxST, int clientsNumber, int simTime) {
        this.minAR = minAR;
        this.maxAR = maxAR;
        this.minST = minST;
        this.maxST = maxST;
        this.clientsNumber = clientsNumber;
        this.simTime = simTime;

        for (int i = 0; i < 5; i++) {
            float ST; //in seconds
            ST = (new Random().nextFloat() * (maxST - minST) + minST);
            ST = (float) round(ST, 2);
            list.addCoada(new Coada("casa " + (i + 1), ST));
        }
    }

    synchronized public int choose() {
        int min = 20, index = 0;
        for (int i = 0; i < 5; i++) {
            if (list.getCoada(i).getServiceTime() * list.getCoada(i).getSize() < list.getCoada(index).getServiceTime() * list.getCoada(index).getSize()) {
                index = i;
            } else if (list.getCoada(i).getServiceTime() * list.getCoada(i).getSize() == list.getCoada(index).getServiceTime() * list.getCoada(index).getSize()) {
                if (list.getCoada(i).getSize() < list.getCoada(index).getSize()) {
                    index = i;
                }
            }
        }
        return index;
    }

    synchronized public void generate(long start) throws InterruptedException {
        long current;
        int noList, size;
        float serviceTime, time;
        for (int i = 0; i < clientsNumber; i++) {
            int AR = new Random().nextInt(maxAR - minAR) + minAR; // in milliseconds
            noList = choose();

            serviceTime = list.getCoada(noList).getServiceTime();
            size = list.getCoada(noList).getCoada().size();
            Thread.sleep(AR);
            current = System.currentTimeMillis();
            time = (float) (current - start) / 1000;

            Client c = new Client(id, (float) round(time, 2), (float) round((size + 1) * serviceTime, 2));
            System.out.println("Clientul " + id + " a fost adaugat la casa " + (noList + 1));
            list.getCoada(noList).addClient(c);
            id++;

//          System.out.println(c);
        }
    }

    private static double round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
