import java.util.ArrayList;
import java.util.List;

public class Producator {
    private List<Coada> list = new ArrayList<>();

    public synchronized void addCoada(Coada queue) {
        this.list.add(queue);
    }

    public synchronized Coada getCoada(int i) {
        return this.list.get(i);
    }
}
