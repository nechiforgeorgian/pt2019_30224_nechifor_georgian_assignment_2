public class Client {
    private int id;
    private float arrivalTime;
    private float serviceTime; // timpul pe care il asteapta

    Client(int id, float arrivalTime, float serviceTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    int getId() {
        return id;
    }

    public String toString() {
        return "Client: " + this.id + " " + "AR: " + this.arrivalTime + " ST: " + this.serviceTime;
    }
}
