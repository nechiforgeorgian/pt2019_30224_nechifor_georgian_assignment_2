import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Coada extends Thread {
    private BlockingQueue<Client> coadaClienti = new ArrayBlockingQueue<>(15);
    private float serviceTime;

    Coada(String name, float serviceTime) {
        setName(name);
        this.serviceTime = serviceTime;
    }

    @Override
    public void run() {
        try {
            while (true) {
                deleteClient();
                Thread.sleep((int) (serviceTime * 1000));
            }
        } catch (InterruptedException e) {
            System.out.println("Intrerupere");
            System.out.println(e.toString());
        }
    }

    synchronized void addClient(Client c) {
        for (Client i : coadaClienti) {
            if (c.getId() == i.getId()) {
                System.out.println("Client with same id.");
                return;
            }
        }
        this.coadaClienti.offer(c);
        notifyAll();
    }

    private void deleteClient() throws InterruptedException {
        if (coadaClienti.isEmpty())
            return;
        Client client = coadaClienti.poll();
        if (!client.equals(null))
            System.out.println(client.getId() + " has been served by " + getName());
    }

    synchronized BlockingQueue<Client> getCoada() {
        return this.coadaClienti;
    }

    synchronized float getServiceTime() {
        return serviceTime;
    }

    synchronized int getSize() {
        return coadaClienti.size();
    }

    @Override
    public synchronized String toString() {
        String str = new String();
        for (Client c : coadaClienti) {
            str += c.toString() + "\n";
        }
        return str;
    }

}
